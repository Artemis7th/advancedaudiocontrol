# AdvancedAudioControl

## Getting started

I made this in response to a post a facebook post a friend sent me. It is written in PowerShell and compiled into an EXE with PowerShell Studio. The application is fully functional so use it if you like.
<br>
The EXE is unsigned so your antivirus may not like it. You also probably need PowerShell 5 and .NET 4.7 at least to run it.

## Screenshots
![Main application window](/images/MainWindow.PNG)
<br>
Main application window
<br>
<br>
![Randomized application window](/images/MainWindowsRandom.PNG)
<br>
Randomized application window
<br>
<br>
![Settings window](/images/Settings.PNG)
<br>
Settings window
<br>
<br>
![About window](/images/About.PNG)
<br>
About window